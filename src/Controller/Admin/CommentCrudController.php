<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('author'),
            TextField::new('text'),
            TextField::new('email'),

            ImageField::new('photoFilename')->setBasePath("/uploads/photos")->setLabel('photo'),
            DateTimeField::new('createdAt'),
            AssociationField::new('conference'),
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('conference')
            ;
    }
}
