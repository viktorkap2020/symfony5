git pull

# поиск и установка необходимых пакетов для работы с проектом.
# запуск обязательный!!!!
composer install

# docker ps -qa - показать все контейнеры докера
# Останавливаем все контейнеры докера, чтобы не получилось, что ты запускаешь БД, а соответствующий порт занят чужим контейнером
docker stop $(docker ps -qa)

# Запуск базы данных
docker-compose up

# Запуск psql(программа для консольного просматривания БД)
docker-compose exec database sh -c 'PGPASSWORD=$POSTGRES_PASSWORD psql --user=main --dbname=main'

# Накатываем миграции
echo "yes" | php bin/console doctrine:migrations:migrate

# Делать каждый раз с нового компа
 INSERT INTO admin (id, username, roles, password) VALUES (nextval('admin_id_seq'), 'admin', '["ROLE_ADMIN"]', '$argon2id$v=19$m=65536,t=4,p=1$IdTpxDFfCxkmzF1Mr02t4Q$VGMoQp7BDflJdhS0Mk5V47MGl5MDYKIZEFUfmKDwDX4');

# Вход в админку : login - admin ; password - admin ;
